(function() {
var funcs = {}

funcs["www.shdfz.net:8090/"] = (function(){
    alert("主页\n插件检查已去除")
    $('#PopNeedPlugins').remove();
    $('.modal-backdrop').remove();
})

funcs["www.shdfz.net:8090/frame/listen/single_train.do"] = (function () {
    alert("听力-单项训练\n答案已开启")
    toSingleTrain = function (trainCount){

        //记录所选年级和所选教材
        setCookie("listen_grade_single",$("#iGradeId").val());
        setCookie("listen_type_single",$("#iTypeId").val());

        $("#wordlist").html('<p align="center"><img src="/skins/default/img/spinner.gif" width="16" height="16" border="0" align="absmiddle" title="加载中,请稍后..."> Loading...</p>');
        $.ajax({
            cache: false,
            type: "POST",
            url:"single_train.dx?action=toSingleTrain",
            data:{"iGradeId":$("#iGradeId").val(),"iTypeId":$("#iTypeId").val(),"trainCount":trainCount},
            async: false,
            error: function(request) {	popErrorAJAX(request, "加载听力列表出错");	},

            success: function(data) {

                if (data==-9){
                    popTimeout();
                    return false;
                }

                $("#wordBookContent").css({"display":"none"});
                $("#wordListContent").css({"display":""});
                var counts = data.split("#PUJIU#")[1]-1;


                $("#clickNum").html(1);
                $("#sumCount").html(counts);
                $("#nowCount").html("01");
                $("#sCount").val(trainCount+" 道题");
                $("#word_list").html(data.split("#PUJIU#")[0]);

                //计时器
                $('#runTimes').val(0);
                clearInterval(runTimes);
                runTimes = setInterval("$('#runTimes').val(parseInt($('#runTimes').val(),10)+1)",1000);

            }
        });

        jwplayer("mediaplayer").setup( {
            flashplayer: "/jwplayer/jwplayer.swf",
            image: "jwplayer.jpg",
            controlbar: "bottom",
            file: "jwplayer.mp4",
            width: "1",
            height: "1",
            volume: "100"
        });

        jwplayer("mediaplayer").onError(function (obj) { popInfo("加载音视频文件失败!!!" + obj.message); })

        $("#mediaplayer").css({"display":"", "width":"0px", "height":"0px", "overflow":"hidden"})

        //播放完成
        jwplayer("mediaplayer").onComplete(function() {

            var nowCount = parseInt($("#nowCount").html(),10)-1;
            var sumCount = parseInt($("#sumCount").html(),10)-1;
            var word = $("tr:eq("+nowCount+")").children("td:eq(1)").html();

            var className = $("#playWord").children("i").attr("class"); //获取是否自动播放
            //自动播放状态
            if (className == "icon-pause") {
                //if (nowCount<sumCount) {
                //$("#nextWord").click();
                //} else {
                $("#playWord").children("i").removeClass("icon-pause");
                $("#playWord").children("i").addClass("icon-play");
                //}
            }
        });


        listenArray = new Array(); //声明一个一位数组


        for(var i=0; i<=parseInt($("tr").index($("#word_list").find("tr:last")),10); i++){        //一维长度为10
            listenArray[i] = $("tr:eq("+i+")").children("td:last").html()+"#PUJIU#[WRONG]"+"#PUJIU#"+$("tr:eq("+i+")").children("td:first").html()+"#PUJIU#"+$("tr:eq("+i+")").children("td:eq(6)").html()+"#PUJIU#";
        }



        $("#word_list").find('tr').unbind("click");
        $("#word_list").find('tr').click(function() {
            $("#clickNum").html(($("tr").index($(this))));

            var questionCount = $(this).children("td:eq(1)").html();
            var nowCount = $("#nowCount").html();

            if (nowCount.split("chr()"))

            if (questionCount>0) {

                $("#nowCount").html($(this).children("td:eq(0)").html()+"1");

                $("#question_info").html($(this).children("td:eq(1)").html());

                var optionA = $(this).children("td:eq(2)").html();
                var optionB = $(this).children("td:eq(3)").html();
                var optionC = $(this).children("td:eq(4)").html();
                var optionD = $(this).children("td:eq(5)").html();
                var typeId = $(this).children("td:eq(7)").html();
                var listenId = $(this).children("td:last").html();

                var optionTemp = "";

                //#####    ###   #       ######   #####     #    #######
                //     #  #   #  #       #     # #     #   # #      #
                //       #     # #       #     # #        #   #     #
                //       #     # #       #     # #       #     #    #
                //       #     # #       #     # #       #######    #
                //     #  #   #  #       #     # #     # #     #    #
                //#####    ###   ####### ######   #####  #     #    #

                // add answer
                optionTemp += "<p>The answer is " + $(this).children("td:eq(6)").html() + ".</p>";

                if (typeId==4) {
                    if (optionA != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='A' name='"+listenId+"'\> A: <img src='/common/getListenImg.asp?picId="+optionA+"' style='width:150px; height:150px;' onerror='/skins/default/img/empty.gif' /></label>";
                    }

                    if (optionB != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='B' name='"+listenId+"'\> B: <img src='/common/getListenImg.asp?picId="+optionB+"' style='width:150px; height:150px;' onerror='/skins/default/img/empty.gif' /></label>";
                    }

                    if (optionC != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='C' name='"+listenId+"'\> C: <img src='/common/getListenImg.asp?picId="+optionC+"' style='width:150px; height:150px;' onerror='/skins/default/img/empty.gif' /></label>";
                    }

                    if (optionD != "" && optionD != "none") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='D' name='"+listenId+"'\> D: <img src='/common/getListenImg.asp?picId="+optionD+"' style='width:150px; height:150px;' onerror='/skins/default/img/empty.gif' /></label>";
                    }
                } else {
                    if (optionA != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='A' name='"+listenId+"'\> A: "+optionA+"</label>";
                    }

                    if (optionB != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='B' name='"+listenId+"'\> B: "+optionB+"</label>";
                    }

                    if (optionC != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='C' name='"+listenId+"'\> C: "+optionC+"</label>";
                    }

                    if (optionD != "" && optionD != "none") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='D' name='"+listenId+"'\> D: "+optionD+"</label>";
                    }
                }

                $("#option_info").html(optionTemp);

            } else {

                $("#nowCount").html($(this).children("td:eq(0)").html());

                $("#question_info").html($(this).children("td:eq(1)").html());

                var optionA = $(this).children("td:eq(2)").html();
                var optionB = $(this).children("td:eq(3)").html();
                var optionC = $(this).children("td:eq(4)").html();
                var optionD = $(this).children("td:eq(5)").html();
                var typeId = $(this).children("td:eq(7)").html();
                var listenId = $(this).children("td:last").html();

                var optionTemp = "";

                //#####    ###   #       ######   #####     #    #######
                //     #  #   #  #       #     # #     #   # #      #
                //       #     # #       #     # #        #   #     #
                //       #     # #       #     # #       #     #    #
                //       #     # #       #     # #       #######    #
                //     #  #   #  #       #     # #     # #     #    #
                //#####    ###   ####### ######   #####  #     #    #

                // add answer
                optionTemp += "<p>The answer is " + $(this).children("td:eq(6)").html() + ".</p>";

                if (typeId==4) {
                    if (optionA != "") {
                        optionTemp += "<label style=\"float:left;border:#000 solid 0px;  line-height:75px; margin:5px;\"><div style='float:left;'><input class=\"myAnswer\" type=\"radio\" value=\"A\" name=\""+listenId+"\"\></div><div style='float:left; width:170px;'> A: <img src=\"/common/getListenImg.asp?picId="+optionA+"\" style=\"width:140px; height:140px;\" /></div></label>";
                    }

                    if (optionB != "") {
                        optionTemp += "<label style=\"float:left;border:#000 solid 0px;  line-height:75px; margin:5px;\"><div style='float:left;'><input class=\"myAnswer\" type=\"radio\" value=\"B\" name=\""+listenId+"\"\></div><div style='float:left; width:170px;'> B: <img src=\"/common/getListenImg.asp?picId="+optionB+"\" style=\"width:140px; height:140px;\" /></div></label>";
                    }

                    if (optionC != "") {
                        optionTemp += "<label style=\"float:left;border:#000 solid 0px;  line-height:75px; margin:5px;\"><div style='float:left;'><input class=\"myAnswer\" type=\"radio\" value=\"C\" name=\""+listenId+"\"\></div><div style='float:left; width:170px;'> C: <img src=\"/common/getListenImg.asp?picId="+optionC+"\" style=\"width:140px; height:140px;\" /></div></label>";
                    }

                    if (optionD != "" && optionD != "none") {
                        optionTemp += "<label style=\"float:left;border:#000 solid 0px;  line-height:75px; margin:5px;\"><div style='float:left;'><input class=\"myAnswer\" type=\"radio\" value=\"D\" name=\""+listenId+"\"\></div><div style='float:left; width:175px;'> D: <img src=\"/common/getListenImg.asp?picId="+optionD+"\" style=\"width:140px; height:140px;\" /></div></label>";
                    }
                } else {
                    if (optionA != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='A' name='"+listenId+"'\> A: "+optionA+"</label>";
                    }

                    if (optionB != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='B' name='"+listenId+"'\> B: "+optionB+"</label>";
                    }

                    if (optionC != "") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='C' name='"+listenId+"'\> C: "+optionC+"</label>";
                    }

                    if (optionD != "" && optionD != "none") {
                        optionTemp += "<label><input class='myAnswer' type='radio' value='D' name='"+listenId+"'\> D: "+optionD+"</label>";
                    }
                }

                //alert(optionTemp)
                $("#option_info").html(optionTemp);
            }


            var isScore = 0;
            $(".myAnswer").unbind("click");
            $(".myAnswer").click(function(){
                var clickNum = parseInt($("#clickNum").html(),10);

                var myanswer = $(this).val();
                var answer = $("tr:eq("+clickNum+")").find("td:eq(6)").html();

                if (trimStr(myanswer) != trimStr(answer)) {
                    listenArray[clickNum] = listenArray[clickNum].replace("[RIGHT]","[WRONG]");
                } else {
                    listenArray[clickNum] = listenArray[clickNum].replace("[WRONG]","[RIGHT]");
                }

                var arrayTemp = listenArray[clickNum].split("#PUJIU#")[0]+"#PUJIU#"+listenArray[clickNum].split("#PUJIU#")[1]+"#PUJIU#"+listenArray[clickNum].split("#PUJIU#")[2]+"#PUJIU#"+answer+"#PUJIU#"+myanswer;

                listenArray[clickNum] = arrayTemp;


                if (clickNum==listenArray.length-1) {

                    if (isScore=="0") {
                        isScore = 1;
                        var rightWords = 0;
                        var wrongWords = 0;

                        for(var i=0; i<listenArray.length;i++) {
                            if (listenArray[i].split("#PUJIU#")[1] == "[RIGHT]") {
                                rightWords++;
                            } else {
                                wrongWords++;
                            }
                        }

                        var score = 0;

                        if (rightWords > 0) {
                            score = parseInt((rightWords/listenArray.length)*100,10);
                        }

                        reportTitle = "听力训练-训练成绩";
                        reportContentTitle = "单项训练";
                        reportContent = $("#sGradeName").val()+"-"+$("#sTypeName").val()+"-"+$("#sCount").val();
                        reportNums = listenArray.length;
                        reportTimes = $("#runTimes").val()+" 秒";
                        reportRight = rightWords;
                        reportWrong = wrongWords;
                        reportScore = score+" 分";

                        //新增到训练表
                        insertListenTrain($("#runTimes").val(), score, listenArray);

                    }

                } else {
                    if($("#pj_iAutoNext_tl").val()==1) {
                        setTimeout("$('#nextWord').click()",1000);
                    }
                }
            });


            $('input[type=radio]').uniform();
            if (nowListenId != listenId) {
                nowListenId = listenId;
                jwplayer("mediaplayer").load({file:ADUIO_PATH+"/"+SPEAKER+"/"+SPEED+"/listen/"+listenId+".mp3", image:''});
                jwplayer("mediaplayer").play();
            }

        });



        //点击自动播放/暂停
        $("#playWord").unbind("click");
        $("#playWord").click(function(){
            nowListenId = "0";

            var className = $(this).children("i").attr("class");
            var nowCount = parseInt($("#clickNum").html(),10);

            if (className=="icon-play") {
                //自动播放
                $(this).children("i").removeClass("icon-play");
                $(this).children("i").addClass("icon-pause");
                $("tr:eq("+nowCount+")").click();

            } else {
                //暂停播放
                jwplayer().stop();
                $(this).children("i").removeClass("icon-pause");
                $(this).children("i").addClass("icon-play");

            }
        });

        nowListenId = $("tr:first").find("td:last").html();
        $("tr:first").click();

        if($("#pj_iAutoPlay_tl").val()==1) {
            setTimeout("$('#playWord').click()",1000);
        }
    }
})

var func = funcs[window.location.href.split("//")[1]];
if (func != null) {
    func();
    $('.headerleft').find('a').find('img').attr('src', "https://c0ldcat.github.io/No-Pujiu-Education/cat.png");
} else {
    alert("未知页面");
}
})();
