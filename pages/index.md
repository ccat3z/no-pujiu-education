# No-Pujiu-Education

This is a plug-in written in JavaScript used to finish the boring Pujiu homework in one second.

## How to use it?

Drag 
[me](javascript:var e = document.createElement('script');e.src='https://c0ldcat.github.io/No-Pujiu-Education/no-pujiu-education.min.js';document.body.appendChild(e);)
into your bookmarks and click it when you want to modfiy the website.

You have to init this plug-in each time you reload any page.

And if the plug-in load successfully, your avatar will become a flat white cat.

## Who write it?

Just a little cat. :D

## Features

* 主页免插件验证
* 听力训练-单项训练显示答案
* More...

## Screencast

![screencast](https://c0ldcat.github.io/No-Pujiu-Education/screencast.gif)
